package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * ArrayIterator class that provides an Iterator for any array.
 * @author Pipatpol Tanavongchinda
 *
 * @param <T> is a type parameter.
 */
public class ArrayIterator<T> implements Iterator<T> {
	/** An array that can contain any type. */
	private T[] array;
	/** A cursor that remember its position in the collection.  */
	private int cursor = 0;
	
	/**
	 * Create an ArayIterator that provides iterator to array class.
	 * @param arr is the array that want to add iterator method.
	 */
	public ArrayIterator( T[] arr)
	{
		array = arr;
	}
	/**
	 * Check if this array has next item.
	 * @return Returns true if next() can return another non-null array element,
	 * false if no more elements.
	 */
	public boolean hasNext()
	{
		for( int run = cursor ; run < array.length ; run++)
		{
			if ( array[run] == null )
			{
				continue;
			}
			else
			{
				return true;
			}
		}
		return false;
	}
	/**
	 * Return next non-null item if exist.
	 * @return Return the next non-null element in the array.
	 * If there are no more elements, throws NoSuchElementException.
	 */
	public T next (  )
	{
		while(hasNext())
		{
			if(array[cursor] == null)
			{
				cursor++;
			}
			else{
				return array[cursor++];
			}
		}
		throw new NoSuchElementException();
	}
	/**
	 * Remove most recent element returned by next() from the array by
	 * setting it to null. 
	 */
	public void remove()
	{
		array[cursor-1] = null;
	}

}

